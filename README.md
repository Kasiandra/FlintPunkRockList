# FlintPunkRockList

Schleife um alle Alben aus der Liste von https://www.plastic-bomb.eu/2021/es-gibt-aber-auch-kaum-bands-in-denen-frauen-spielen/ abzugreifen und diese in ein File zu schreiben.
<br>
### Play random albums with mpv

```
mpv --shuffle $(curl -s "https://git.elektrollart.org/Elektroll/FlintPunkRockList/raw/branch/master/playlist.txt" | tr '\n' ' ')
```